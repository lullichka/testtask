package com.example.lulichka.testtask;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

class DataBase {
    private static final String TAG = DataBase.class.getSimpleName();

    static final String TABLE_RESULTS = "results";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_TEXT = "text";
    static final String COLUMN_PICTURE_URL = "picture_url";
    static final String COLUMN_FAVORITE = "favorite";
    private static final String DATABASE_CREATE = "create table "
            + TABLE_RESULTS + "( " + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_TEXT
            + " text, "
            + COLUMN_FAVORITE
            + " integer, "
            + COLUMN_PICTURE_URL
            + " text"
            + ");";

    static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }
    static void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.w(TAG,
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_RESULTS);
        onCreate(sqLiteDatabase);}

    }