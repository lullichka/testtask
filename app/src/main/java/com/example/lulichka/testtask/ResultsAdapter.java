package com.example.lulichka.testtask;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.ResultsViewHolder> {
    private static final String PATH_TO_IMG = "path";
    private Context mContext;
    private Cursor mCursor;

    ResultsAdapter(Context context, Cursor cursor) {
        mCursor = cursor;
        mContext = context;
    }

    Cursor swapCursor(Cursor cursor) {
        if (mCursor == cursor) {
            return null;
        }
        Cursor oldCursor = mCursor;
        this.mCursor = cursor;
        if (cursor != null) {
            this.notifyDataSetChanged();
        }
        return oldCursor;
    }

    @Override
    public ResultsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View issueView = inflater.inflate(R.layout.item_search, parent, false);
        return new ResultsViewHolder(issueView);
    }

    @Override
    public void onBindViewHolder(ResultsViewHolder holder, int position) {
        mCursor.moveToPosition(position);
        holder.setContextAndCursor(mContext, mCursor);
        holder.textResult.setText(mCursor.getString(mCursor.getColumnIndex(DataBase.COLUMN_TEXT)));
        String imageCategory = mCursor.getString(mCursor.getColumnIndex(DataBase.COLUMN_PICTURE_URL));
        Picasso.with(mContext)
                .load(imageCategory)
                .fit()
                .centerCrop()
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewResult);
    }

    @Override
    public int getItemCount() {
        return (mCursor == null) ? 0 : mCursor.getCount();
    }

    static class ResultsViewHolder extends RecyclerView.ViewHolder {
        TextView textResult;
        ImageView imageViewResult;
        CheckBox checkBox;
        Context context;
        Cursor cursor;

        private ResultsViewHolder setContextAndCursor(Context context, Cursor cursor){
            this.cursor = cursor;
            this.context = context;
            return this;
        }
        private  ResultsViewHolder(View itemView) {
            super(itemView);
            textResult = (TextView) itemView.findViewById(R.id.tvText);
            imageViewResult = (ImageView) itemView.findViewById(R.id.ivImg);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    showAddToFavoriteDialog(getAdapterPosition());
                    checkBox.setChecked(false);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    cursor.moveToPosition(position);
                    String path = cursor.getString(cursor.getColumnIndex(DataBase.COLUMN_PICTURE_URL));
                    ImageFragment imageFragment = new ImageFragment();
                    Bundle args = new Bundle();
                    args.putString(PATH_TO_IMG, path);
                    imageFragment.setArguments(args);
                    ((MainActivity) context).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.activity_main, imageFragment)
                            .addToBackStack(null).commit();
                }
            });
        }
        private void showAddToFavoriteDialog(final int position) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.dialog_exit, null, false);
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setCancelable(true);
            builder.setView(view);
            final AlertDialog exitDialog = builder.create();
            view.findViewById(R.id.cancel_exit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    exitDialog.dismiss();
                }
            });
            view.findViewById(R.id.confirm_exit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cursor.moveToPosition(position);
                    long id = cursor.getLong(cursor.getColumnIndex(DataBase.COLUMN_ID));
                    ResultsRepository.getInstance().addToFavorites(context, id);
                    exitDialog.dismiss();
                }
            });
            exitDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            exitDialog.show();
        }
    }

}
