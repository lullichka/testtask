package com.example.lulichka.testtask;


import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class ResultsRepository {
    private static final String TAG = ResultsRepository.class.getSimpleName();
    private static ResultsRepository INSTANCE = new ResultsRepository();

    static ResultsRepository getInstance() {
        return INSTANCE;
    }

    void search(final Context context, String query, int index) {
        UIUtils.showLoading(context);
        RestService.Service.getSearchResults(
                context.getString(R.string.API_key),
                context.getString(R.string.cx_engine),
                query,
                index
        ).enqueue(new Callback<SearchResponse>() {
                      @Override
                      public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                          SearchResponse body = response.body();
                          if (body != null && body.items != null) {
                              Log.d(TAG, String.valueOf(body.items.size()));
                              for (SearchResponse.Item item : body.items) {
                                  if (item.pagemap.cseImage != null && item.title != null) {
                                      String title = item.title;
                                      String imageUrl = item.pagemap.cseImage.get(0).src;
                                      ContentValues values = new ContentValues();
                                      values.put(DataBase.COLUMN_TEXT, title);
                                      values.put(DataBase.COLUMN_PICTURE_URL, imageUrl);
                                      values.put(DataBase.COLUMN_FAVORITE, 0);
                                      Uri uri = context.getContentResolver().insert(
                                              ResultContentProvider.CONTENT_URI, values);
                                      Log.d(TAG, "inserted " + uri.getPath());
                                  }
                              }
                          }
                          UIUtils.hideLoading();
                          EventBus.getDefault().post(new LoadingDoneMessage());
                      }

                      @Override
                      public void onFailure(Call<SearchResponse> call, Throwable t) {
                          Log.d(TAG, "Failure " + t.getMessage());
                      }
                  }
        );
    }

    void addToFavorites(Context context, long id) {
        Uri uri = Uri.parse(ResultContentProvider.CONTENT_URI + "/" + id);
        ContentValues cv = new ContentValues();
        cv.put(DataBase.COLUMN_FAVORITE, 1);
        context.getContentResolver().update(uri, cv, null, null);
    }

    void clear(Context context) {
        Uri uri = ResultContentProvider.CONTENT_URI;
        context.getContentResolver().delete(uri, DataBase.COLUMN_FAVORITE + "=" + 0, null);
    }
}



