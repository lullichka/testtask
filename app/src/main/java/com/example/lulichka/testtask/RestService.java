package com.example.lulichka.testtask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;


public class RestService {
    public static WebAPI Service = init();

    private static WebAPI init() {
        OkHttpClient client = new OkHttpClient.Builder().build();
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.googleapis.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(WebAPI.class);

    }

    public interface WebAPI {
        @GET("customsearch/v1/")
        Call<SearchResponse> getSearchResults(@Query("key") String apiKey,
                                              @Query("cx") String e,
                                              @Query("q") String query,
                                              @Query("start") int startIndex);
    }
}
