package com.example.lulichka.testtask;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


class ResultsSQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String TAG = ResultsSQLiteOpenHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "results.db";
    private static final int DATABASE_VERSION = 1;


    ResultsSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        DataBase.onCreate(sqLiteDatabase);
        Log.d(TAG, "database created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        DataBase.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }
}
