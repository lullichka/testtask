package com.example.lulichka.testtask;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ImageFragment extends Fragment {
    private static final String PATH_TO_IMG = "path";

    String mPath;

    public ImageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPath = getArguments().getString(PATH_TO_IMG);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_image, container, false);
        ImageView imageView = (ImageView) v.findViewById(R.id.imageView);
        Picasso.with(getActivity())
                .load(mPath)
                .fit()
                .centerInside()
                .placeholder(R.drawable.placeholder)
                .into(imageView);
        return v;
    }

}
