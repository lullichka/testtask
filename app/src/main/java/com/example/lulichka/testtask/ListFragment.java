package com.example.lulichka.testtask;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.lulichka.testtask.DataBase.COLUMN_FAVORITE;


public class ListFragment extends Fragment implements SearchView.OnQueryTextListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = ListFragment.class.getSimpleName();
    private ResultsAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private ResultsRepository mResultsRepository;
    private boolean mLoading = true;
    String mQuery;
    int mIndex;

    @BindView(R.id.search_view)
    SearchView mSearchBox;
    @BindView(R.id.rv)
    RecyclerView mRecyclerView;

    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getSupportLoaderManager().initLoader(0, null, this);
        mResultsRepository = ResultsRepository.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, v);
        mSearchBox = (SearchView) v.findViewById(R.id.search_view);
        mSearchBox.setOnQueryTextListener(this);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ResultsAdapter(getActivity(), null);
        mRecyclerView.setAdapter(mAdapter);
        EventBus.getDefault().register(this);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.addOnScrollListener(mRecyclerViewOnScrollListener);
    }

    private RecyclerView.OnScrollListener mRecyclerViewOnScrollListener =
            new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    int pastVisiblesItems, visibleItemCount, totalItemCount;
                    if (dy > 0) {
                        visibleItemCount = mLayoutManager.getChildCount();
                        totalItemCount = mLayoutManager.getItemCount();
                        pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                        if (mLoading) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                mLoading = false;
                                mResultsRepository.search(getActivity(), mQuery, mIndex);
                            }
                        }
                    }
                }
            };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bndl) {
        String[] projection = {DataBase.COLUMN_ID, DataBase.COLUMN_TEXT, DataBase.COLUMN_PICTURE_URL};
        return new CursorLoader(getActivity(),
                ResultContentProvider.CONTENT_URI, projection, COLUMN_FAVORITE + " = " + 0, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mQuery = query;
        mResultsRepository.clear(getActivity());
        mIndex = 1;
        mResultsRepository.search(getActivity(), mQuery, mIndex);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Subscribe
    public void onLoaded(LoadingDoneMessage message) {
        getActivity().getSupportLoaderManager().getLoader(0).forceLoad();
        mIndex += 10;
        mLoading = true;
    }
}