package com.example.lulichka.testtask;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResponse {

    @SerializedName("items")
    @Expose
    public List<Item> items = null;



    public class CseImage {

        @SerializedName("src")
        @Expose
        public String src;

    }


    public class Item {

        @SerializedName("title")
        @Expose
        public String title;
        @Expose
        @SerializedName("pagemap")
        public Pagemap pagemap;

    }

    public class Pagemap {


        @SerializedName("cse_image")
        @Expose
        public List<CseImage> cseImage = null;


    }
}













