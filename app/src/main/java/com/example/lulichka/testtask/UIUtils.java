package com.example.lulichka.testtask;

import android.app.ProgressDialog;
import android.content.Context;

class UIUtils {

    private static ProgressDialog mProgressDialog;

    static void showLoading(Context context) {
        if (context == null) {
            return;
        }
        hideLoading();
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    static void hideLoading() {
        if (mProgressDialog != null
                && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
